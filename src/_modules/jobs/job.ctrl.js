(function () {
    angular
        .module('jobs')
        .controller('jobCtrl', [
            'appSvc', 'jobService', '$mdDialog',
            jobCtrl
        ]);

    function jobCtrl(appSvc, jobService, $mdDialog) {
        var self = this;

        this.orderBy = 'created_at';

        appSvc.header = 'Job Listings';

        self.list = [];

        jobService
            .loadAllJobs()
            .then(function (jobs) {
                self.list = [].concat(jobs.data);
            });

        this.selected = [];

        this.query = {
            order: 'interview_date',
            limit: 5,
            page: 1
        };

        this.showDetails = function (job, ev) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'src/_modules/jobs/jobDialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: true,
                locals: {
                    job: job
                },
            })
        };
    }

})();


function DialogController($scope, $sce, $mdDialog, job) {
    $scope.job = job;
    
    $scope.hide = function () {
        $mdDialog.hide();
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.answer = function (answer) {
        $mdDialog.hide(answer);
    };
}